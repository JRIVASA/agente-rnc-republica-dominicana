VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Funciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpDefault _
As String, ByVal lpReturnedString As String, ByVal _
nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName _
As String, ByVal lpKeyName As Any, ByVal lpstring _
As Any, ByVal lpFileName As String) As Long

Public Function sGetIni(SIniFile As String, SSection As String, SKey _
As String, SDefault As String) As String
    
    Const MaxLen = 10000
    
    Dim sTemp As String * MaxLen
    Dim NLength As Integer
    
    sTemp = Space$(MaxLen)
    NLength = GetPrivateProfileString(SSection, SKey, SDefault, sTemp, MaxLen - 1, SIniFile)
    
    sGetIni = Left$(sTemp, NLength)
    
End Function

Public Function sWriteIni(SIniFile As String, SSection As String, SKey _
        As String, SData As String) As String
    Dim NLength As Integer
    
    NLength = WritePrivateProfileString(SSection, SKey, SData, SIniFile)
End Function

Public Function SafeCreateObject(pClass As String, Optional pServerName As String = "") As Object
    
    On Error GoTo Error
    
    If Trim(pServerName) = "" Then
        Set SafeCreateObject = CreateObject(pClass)
    Else
        Set SafeCreateObject = CreateObject(pClass, pServerName)
    End If
    
    Exit Function
    
Error:
    
    Debug.Print Err.Description
    
    Set SafeCreateObject = Nothing
    
End Function

Public Sub Ejecutar()
    
    'If sGetIni((App.Path & "\Setup.ini"), "Opciones", "ActualizarDepoprodExistencia", "1") = "1" Then
        Call ProcesarArchivo
    'End If
    
End Sub

Public Sub ProcesarArchivo()
    
    On Error GoTo Error1
    
    Dim Rec_DPxP As New ADODB.Recordset
    Dim Rec_Prod As New ADODB.Recordset
    
    Dim Trans As Boolean
    
    Trans = False
    
    Call ConectarBDD
    
    Dim WholeFileContents As String
    
    Dim LineArray As Variant, FieldArray As Variant
    
    Dim TInicio As Date, TFin As Date
    
    Call CreateFullDirectoryPath(App.Path & "\Procesados\")
    
    TInicio = Now
    
    If PathExists(App.Path & "\DGII_RNC.txt") Then
        WholeFileContents = ReadFileIntoString(App.Path & "\DGII_RNC.txt") 'LoadTextFile(App.Path & "\DGII_RNC.txt")
        'MsgBox Len(WholeFileContents)
        'End
        If Trim(WholeFileContents) = Empty Then
            Err.Raise "999", , "Archivo vacio o Ilegible"
        Else
            LineArray = Split(WholeFileContents, vbLf)
        End If
    Else
        End
    End If
    
    Dim pSoloActivos As Boolean
    pSoloActivos = Val(sGetIni((App.Path & "\Setup.ini"), "Opciones", "SoloActivos", "1")) = 1
    
    Dim SobreescribirDatos As Boolean
    SobreescribirDatos = Val(sGetIni((App.Path & "\Setup.ini"), "Opciones", "SobreescribirDatos", "0")) = 1
    
    ENT.BDD.CommandTimeout = 0
    
    Dim mRs As New ADODB.Recordset
    mRs.CursorLocation = adUseClient
    
    If SobreescribirDatos Then
        
        mRs.Open "SELECT * FROM MA_LISTA_RNC_RD WHERE 1 = 2", _
        ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        mRs.ActiveConnection = Nothing
        
    Else
        
        ExecuteSafeSQL "DROP TABLE #TMP_CARGA_LISTA_RNC_RD", ENT.BDD
        
        ENT.BDD.Execute "SELECT * INTO #TMP_CARGA_LISTA_RNC_RD " & _
        "FROM MA_LISTA_RNC_RD WHERE 1 = 2"
        
        mRs.Open "SELECT * FROM #TMP_CARGA_LISTA_RNC_RD WHERE 1 = 2", _
        ENT.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        mRs.ActiveConnection = Nothing
        
    End If
    
    For i = 0 To UBound(LineArray)
        FieldArray = Split(LineArray(i), "|")
        If UBound(FieldArray) > 8 Then
            If (Not pSoloActivos) Or (pSoloActivos And UCase(FieldArray(9)) = UCase("ACTIVO")) Then
                With mRs
                    .AddNew
                    Debug.Print i
                    !Linea = i
                    !RNC = FieldArray(0)
                    !Estatus = FieldArray(9)
                    !Raw_Text = LineArray(i)
                End With
            End If
        End If
    Next
    
    'TFin = Now
    
    'TInicio = Now
    
    If Not SobreescribirDatos Then ' Precargar datos temporales en la BD.
        mRs.ActiveConnection = ENT.BDD
        mRs.UpdateBatch
    End If
    
    TFin = Now
    
    'Debug.Print SQL1
    
    Debug.Print "Tiempo de carga: " & FormatDateTime(CDate(TFin - TInicio), vbLongTime)
    
    ENT.BDD.BeginTrans
    
    TInicio = Now
    
    Trans = True
    
    If SobreescribirDatos Then
        
        ENT.BDD.Execute "TRUNCATE TABLE MA_LISTA_RNC_RD"
        mRs.ActiveConnection = ENT.BDD
        mRs.UpdateBatch
        
    Else
        
        ENT.BDD.Execute _
        "INSERT INTO MA_LISTA_RNC_RD " & _
        "(Linea, RNC, Estatus, Raw_Text) " & _
        "SELECT TMP.Linea, TMP.RNC, TMP.Estatus, TMP.Raw_Text " & _
        "FROM #TMP_CARGA_LISTA_RNC_RD TMP " & _
        "LEFT JOIN MA_LISTA_RNC_RD DATOS " & _
        "ON TMP.RNC COLLATE MODERN_SPANISH_CI_AS " & _
        "= DATOS.RNC COLLATE MODERN_SPANISH_CI_AS " & _
        "WHERE DATOS.RNC COLLATE MODERN_SPANISH_CI_AS IS NULL ", Records
        
        ENT.BDD.Execute _
        "UPDATE DATOS SET " & _
        "Linea = TMP.Linea, " & _
        "RNC = TMP.RNC, " & _
        "Estatus = TMP.Estatus, " & _
        "Raw_Text = TMP.Raw_Text " & _
        "FROM MA_LISTA_RNC_RD DATOS " & _
        "INNER JOIN #TMP_CARGA_LISTA_RNC_RD TMP " & _
        "ON TMP.RNC COLLATE MODERN_SPANISH_CI_AS " & _
        "= DATOS.RNC COLLATE MODERN_SPANISH_CI_AS " & _
        "WHERE 1 = 1 " & _
        "AND LTRIM(RTRIM(DATOS.RNC)) COLLATE MODERN_SPANISH_CI_AS <> '' " & _
        "AND DATOS.Linea <> -1 ", Records
        
    End If
    
    ENT.BDD.CommitTrans
    
    Call CopyPath(App.Path & "\DGII_RNC.txt", App.Path & "\Procesados\DGII_RNC_" & _
    Format(Now, "YYYY") & "_" & Format(Now, "MM") & "_" & Format(Now, "DD") & "_" & _
    Format(Now, "HH") & "_" & Format(Now, "NN") & "_" & Format(Now, "SS") & ".txt")
    
    KillSecure App.Path & "\DGII_RNC.txt"
    
    TFin = Now
    
    Debug.Print "Tiempo de actualizacion: " & FormatDateTime(CDate(TFin - TInicio), vbLongTime)
    
    Exit Sub
    
Error1:
    
    'Resume ' Debug
    
    If Trans Then
        ENT.BDD.RollbackTrans
    End If
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al Procesar Archivo DGII_RNC.txt: " & Err.Description & " " & "(" & Err.Number & ")[" & Err.Source & "]"
    LogFile LogContent
    
End Sub

Private Sub ConectarBDD()

    On Error GoTo ErrHandler
    
    Dim Setup As String
    
    Setup = App.Path & "\Setup.ini"
    
    Dim SRV_LOCAL As String, SRV_REMOTE As String, PROVIDER_LOCAL As String, _
    UserDB As String, UserPwd As String, NewUser As String, NewPassword As String
    
    'BUSCAR VALORES
    
    SRV_LOCAL = sGetIni(Setup, "Server", "srv_local", "?")
    
    If SRV_LOCAL = "?" Then
        Call MsgBox("El servidor local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    SRV_REMOTE = sGetIni(Setup, "Server", "srv_remote", "?")
    
    If SRV_REMOTE = "?" Then
        Call MsgBox("El servidor remoto no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    PROVIDER_LOCAL = sGetIni(Setup, "Proveedor", "Proveedor", "?")
    
    If PROVIDER_LOCAL = "?" Then
        Call MsgBox("El proveedor de servicio local no se encuentra configurado.", vbCritical, "Error")
        End
    End If
    
    UserDB = sGetIni(Setup, "Server", "User", "?")
    
    If UserDB = "?" Then
        UserDB = "SA"
    End If
    
    UserPwd = sGetIni(Setup, "Server", "Password", "?")
    
    If UserPwd = "?" Then
        UserPwd = ""
    End If
    
    If Not (UCase(UserDB) = "SA" And Len(UserPwd) = 0) Then
        
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If Not mClsTmp Is Nothing Then
            UserDB = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserDB)
            UserPwd = mClsTmp.Decode(gCodProducto, gNombreProducto, gPK, UserPwd)
        End If
        
    End If
    
Retry:
    
    'INICIO CONEXION
    
    SafeItemAssign ENT.BDD.Properties, "Prompt", adPromptNever
    
    If ENT.BDD.State = adStateOpen Then ENT.BDD.Close
    
    ENT.BDD.ConnectionString = "Provider= " & PROVIDER_LOCAL & ";Persist Security Info=True;User ID=" & UserDB & ";Password=" & UserPwd & ";Initial Catalog=VAD10;Data Source=" & SRV_REMOTE
    
    ENT.BDD.CommandTimeout = 0
    
    ENT.BDD.Open
    
    ConectarBaseDatos = True
    
    Exit Sub
    
ErrHandler:
    
    mErrDesc = Err.Description
    mErrNumber = Err.Number
    mErrSrc = Err.Source
    
    If mErrNumber = -2147217843 Then
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo UnhandledErr
        
        TmpVar = mClsTmp.RequestAccess(gCodProducto, gNombreProducto, gPK)
        
        If Not IsEmpty(TmpVar) Then
            UserDB = TmpVar(0): UserPwd = TmpVar(1)
            NewUser = TmpVar(2): NewPassword = TmpVar(3)
            sWriteIni Setup, "Server", "User", NewUser
            sWriteIni Setup, "Server", "Password", NewPassword
            Resume Retry
        End If
        
        Set mClsTmp = Nothing
        
    End If
    
UnhandledErr:
    
    'Call MsgBox(Err.Description, vbCritical, "Error")
    
    LogContent = FormatDateTime(Now, vbGeneralDate) & " - Error al conectar a la BD: " & mErrDesc & " " & "(" & mErrNumber & ")[" & mErrSrc & "]"
    LogFile LogContent
    End
    
End Sub

Public Function BuscarValorBD(Campo As String, SQL As String, Optional pDefault = "", Optional pCn As ADODB.Connection) As String

On Error GoTo Err_Campo

Dim mRs As New ADODB.Recordset

mRs.Open SQL, IIf(pCn Is Nothing, ENT.BDD, pCn), adOpenStatic, adLockReadOnly, adCmdText

If Not mRs.EOF Then

    BuscarValorBD = CStr(mRs.Fields(Campo).Value)

Else
    
    BuscarValorBD = pDefault

End If

mRs.Close

Exit Function

Err_Campo:
Debug.Print Err.Description
Err.Clear
BuscarValorBD = pDefault

End Function

